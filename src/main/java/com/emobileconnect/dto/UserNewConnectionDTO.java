package com.emobileconnect.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserNewConnectionDTO {

	

	@NotEmpty(message = "Please provide user name")
	private String userName;

	private Integer age;

	private String gender;

	//@NotEmpty(message = "Please provide a Mobile Number")
	//@Pattern(regexp = "(^$|[0-9]{10})", message = "Provide valid Mobile Number")
	private Long phoneNo;

	@NotNull(message = "Please provide aadhar number")
	//@Max(value = 10, message = "please provide valid aadhar number")
	private Long aadharNumber;

	@NotEmpty(message = "Please provide Email id")
	@Email
	private String emailId;
	
	
	

	

	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(Long aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(Long phoneNo) {
		this.phoneNo = phoneNo;
	}

	

}
