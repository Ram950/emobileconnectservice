package com.emobileconnect.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.emobileconnect.entity.UserPlans;
import com.emobileconnect.exceptions.UserPlansNotFoundException;
import com.emobileconnect.service.UserPlansService;

import io.swagger.annotations.Api;

@RestController
@Api(value="UserPlansController", tags = {"UserPlansController"})
public class UserPlansController {
	
	@Autowired
	UserPlansService userPlansService;
	
	
	@GetMapping("/getUserPlansByRequestId")
	public ResponseEntity<UserPlans> getUserPlansbyRequestId(@Valid @RequestParam Integer requestId)throws UserPlansNotFoundException {
		
		UserPlans userPlans=userPlansService.getUserPlansbyRequestId(requestId);
		
		return new ResponseEntity<UserPlans>(userPlans,HttpStatus.OK);
		
		
	}

}
