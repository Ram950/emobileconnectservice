package com.emobileconnect.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emobileconnect.dto.UserNewConnectionDTO;
import com.emobileconnect.exceptions.UserNotFoundException;
import com.emobileconnect.service.UserNewConnectionService;

import io.swagger.annotations.Api;

@RestController
@Api(value="UserNewConnectionController", tags = {"UserNewConnectionController"})
@RequestMapping("/userrequestdetails")
public class UserNewConnectionController {
	
	@Autowired
	private UserNewConnectionService userConnectionService;
	
	@PostMapping("/userNewConnection")
	public ResponseEntity<UserNewConnectionDTO> newConnection(@Valid @RequestBody UserNewConnectionDTO userNewConnectionDTO) throws UserNotFoundException{
		UserNewConnectionDTO usersConnecton = userConnectionService.saveNewUserConnection(userNewConnectionDTO);
		return new ResponseEntity<>(usersConnecton, HttpStatus.CREATED);
		
	}
}
