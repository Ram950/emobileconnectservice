package com.emobileconnect.repository;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emobileconnect.dto.UserPlansDto;
import com.emobileconnect.entity.UserPlans;

@Repository
public interface UserPlansRepository extends JpaRepository<UserPlans, Integer> {

	Optional<UserPlans> findUserPlansByRequestId(@Valid Integer requestId);

	
}
