package com.emobileconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emobileconnect.entity.UserDetails;

@Repository
public interface UserNewConnectionRepository extends JpaRepository<UserDetails, Integer> {

}
