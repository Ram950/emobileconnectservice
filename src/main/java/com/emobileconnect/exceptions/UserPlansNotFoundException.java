package com.emobileconnect.exceptions;

public class UserPlansNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UserPlansNotFoundException(String message) {
		super(message);
	}

}
