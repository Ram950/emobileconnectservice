package com.emobileconnect.exceptions;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.emobileconnect.request.ResponseMsg;

@RestControllerAdvice
public class WebRestControllerAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<ResponseMsg> handleNotFoundException(UserNotFoundException ex) {
		ResponseMsg responsemsg = new ResponseMsg();
		responsemsg.setErrcode("123");
		responsemsg.setMessage(ex.getMessage());
		return new ResponseEntity<ResponseMsg>(responsemsg, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = TariffPlansNotFoundException.class)
	public ResponseEntity<ResponseMsg> handleInsuranceNotFoundException(TariffPlansNotFoundException ex) {
		ResponseMsg responsemsg = new ResponseMsg();
		responsemsg.setErrcode("124");
		responsemsg.setMessage(ex.getMessage());
		return new ResponseEntity<ResponseMsg>(responsemsg, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<Object> handleGenericException(Exception ex) {
		ResponseMsg responsemsg = new ResponseMsg();
		responsemsg.setErrcode("126");
		responsemsg.setMessage(ex.getMessage());
		return new ResponseEntity<Object>(responsemsg, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ResponseMsg responsemsg = new ResponseMsg();
		responsemsg.setErrcode("102");
		String allFieldErrors = ex.getBindingResult().getFieldErrors().stream().map(e -> e.getDefaultMessage())
				.collect(Collectors.joining(","));
		responsemsg.setMessage(allFieldErrors);
		return new ResponseEntity<Object>(responsemsg, HttpStatus.BAD_REQUEST);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ConstraintViolationException.class)
	public Map<String, String> handleConstraintViolation(ConstraintViolationException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getConstraintViolations().forEach(cv -> {
			errors.put("message", cv.getMessage());
			errors.put("path", (cv.getPropertyPath()).toString());
		});

		return errors;
	}
	
	public class RestExceptionHandler extends ResponseEntityExceptionHandler {
		@ExceptionHandler(value=UserPlansNotFoundException.class)
		public ResponseEntity<ResponseMsg> handleCustomerNotFoundException(UserPlansNotFoundException userPlansNotFoundException){
			ResponseMsg errorResponse=new ResponseMsg();
			errorResponse.setErrcode("USER REQUESTID");
			errorResponse.setMessage(userPlansNotFoundException.getMessage());
			
			return new ResponseEntity<ResponseMsg>(errorResponse,HttpStatus.BAD_REQUEST);
			
		}
		@Override
		public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException  argInvalidException, HttpHeaders headers,
				HttpStatus status, WebRequest request) {
			ResponseMsg response = new ResponseMsg();
			response.setErrcode("Error Message");
			String allFieldErrors = argInvalidException.getBindingResult().getFieldErrors().stream()
					.map(e -> e.getDefaultMessage()).collect(Collectors.joining(", "));
			response.setMessage(allFieldErrors);
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
	}

}