package com.emobileconnect.batch;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emobileconnect.entity.TariffPlans;
import com.emobileconnect.repository.TariffPlansRepository;

@Component
public class EMobileConnectWriter implements ItemWriter<TariffPlans> {

	@Autowired
	private TariffPlansRepository tariffPlansRepository;
	//writer
		@Override
		@Transactional
		public void write(List<? extends TariffPlans> tariffPlans) throws Exception {
			tariffPlansRepository.saveAll(tariffPlans);
		
	}

}
