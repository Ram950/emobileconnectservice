package com.emobileconnect.batch;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.emobileconnect.entity.TariffPlans;

@Component
public class EMobileConnectKeeperJob extends JobExecutionListenerSupport {

	@Autowired
	JobBuilderFactory jobBuilderFactory;

	@Autowired
	StepBuilderFactory stepBuilderFactory;

	@Value("${input.file}")
	Resource resource;

	@Autowired
	EMobileConnectWriter eMobileConnectWriter;
	
	//batch job

	@Bean(name = "emobileJob")
	public Job accountKeeperJob() {

		Step step = stepBuilderFactory.get("step-1").<TariffPlans, TariffPlans>chunk(1).reader(new EMobileConnectReader(resource))
				.writer(eMobileConnectWriter).build();

		Job job = jobBuilderFactory.get("mobileconnect-job").incrementer(new RunIdIncrementer()).listener(this).start(step)
				.build();

		return job;
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			System.out.println("COMPLETED SUCCESSFULLY");
		}
	}

}
