package com.emobileconnect.batch;


import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.Resource;

import com.emobileconnect.entity.TariffPlans;

public class EMobileConnectReader extends FlatFileItemReader<TariffPlans> {

public EMobileConnectReader(Resource resource) {
	//resource	
			super();
			
			setResource(resource);
			
			DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
			lineTokenizer.setNames(new String[] { "plan_amount", "plan_name", "service_charges", "validity_description", "validity_period" });
			lineTokenizer.setDelimiter(",");
		    lineTokenizer.setStrict(false);
		    
		    BeanWrapperFieldSetMapper<TariffPlans> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
	        fieldSetMapper.setTargetType(TariffPlans.class);

			DefaultLineMapper<TariffPlans> defaultLineMapper = new DefaultLineMapper<>();
			defaultLineMapper.setLineTokenizer(lineTokenizer);
			defaultLineMapper.setFieldSetMapper(fieldSetMapper);
			setLineMapper(defaultLineMapper);
	}

}
