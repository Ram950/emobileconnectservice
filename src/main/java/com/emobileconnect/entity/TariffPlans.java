
package com.emobileconnect.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tariff_plans")
public class TariffPlans {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer planId;
	
	private String planName;
	
	private String validityPeriod;
	
	private String validityDescription;
	
	private Double planAmount;
	
	private Integer serviceCharges;
	
	@OneToMany(mappedBy = "userDetails",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<UserPlans> userplans;

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	
	public String getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(String validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	public String getValidityDescription() {
		return validityDescription;
	}

	public void setValidityDescription(String validityDescription) {
		this.validityDescription = validityDescription;
	}

	public Double getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(Double planAmount) {
		this.planAmount = planAmount;
	}

	public Integer getServiceCharges() {
		return serviceCharges;
	}

	public void setServiceCharges(Integer serviceCharges) {
		this.serviceCharges = serviceCharges;
	}

	public List<UserPlans> getUserplans() {
		return userplans;
	}

	public void setUserplans(List<UserPlans> userplans) {
		this.userplans = userplans;
	}
	

}
