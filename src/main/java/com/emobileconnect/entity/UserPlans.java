package com.emobileconnect.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_plans")
public class UserPlans {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer requestId;

	private String requestStatus;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId")
	private UserDetails userDetails;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "planId")
	private TariffPlans tariffPlans;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	

	

	

}
