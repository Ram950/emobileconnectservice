package com.emobileconnect.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emobileconnect.dto.UserNewConnectionDTO;
import com.emobileconnect.entity.UserDetails;
import com.emobileconnect.repository.UserNewConnectionRepository;
import com.emobileconnect.service.UserNewConnectionService;

@Service
public class UserNewConnectionServiceImpl implements UserNewConnectionService {
	
	@Autowired
	private UserNewConnectionRepository userNewConnectionRepo;

	@Override
	public UserNewConnectionDTO saveNewUserConnection(UserNewConnectionDTO userNewConnectionDto) {
		UserDetails userConnection = new UserDetails();
		
		BeanUtils.copyProperties(userNewConnectionDto, userConnection);
		userConnection.setRequestStatus("In Progress");
		userNewConnectionRepo.save(userConnection);
		return userNewConnectionDto;
	}

}
