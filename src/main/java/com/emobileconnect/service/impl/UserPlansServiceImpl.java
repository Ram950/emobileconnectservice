package com.emobileconnect.service.impl;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emobileconnect.entity.UserPlans;
import com.emobileconnect.exceptions.UserPlansNotFoundException;
import com.emobileconnect.repository.UserPlansRepository;
import com.emobileconnect.service.UserPlansService;


@Service
public class UserPlansServiceImpl implements UserPlansService {
	private static final Logger logger = LoggerFactory.getLogger(UserPlansServiceImpl.class);
	
	@Autowired
	UserPlansRepository userPlansRepository;

	@Override
	public UserPlans getUserPlansbyRequestId(@Valid Integer requestId) throws UserPlansNotFoundException  {
		//UserPlans plans;
		Optional<UserPlans> userPlans= userPlansRepository.findUserPlansByRequestId(requestId);
        if(!userPlans.isPresent()) {
        	logger.warn("Request Id Not Found");
            throw new UserPlansNotFoundException("RequestId  Not Found");
        }
        UserPlans userplan = userPlans.get();
        
        logger.info("UserPalns get by RequestId");
       
        return userplan;
      
	}

	

}
