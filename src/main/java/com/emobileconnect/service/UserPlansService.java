package com.emobileconnect.service;

import java.util.List;

import javax.validation.Valid;

import com.emobileconnect.dto.UserPlansDto;
import com.emobileconnect.entity.UserPlans;
import com.emobileconnect.exceptions.UserPlansNotFoundException;

public interface UserPlansService {

	

	UserPlans getUserPlansbyRequestId(@Valid Integer requestId) throws UserPlansNotFoundException ;

}
