package com.emobileconnect.serviceimpltest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.emobileconnect.EMobileConnectApplication;
import com.emobileconnect.Controllertest.UserPlansControllerTest;
import com.emobileconnect.entity.UserPlans;
import com.emobileconnect.exceptions.UserPlansNotFoundException;
import com.emobileconnect.repository.UserPlansRepository;
import com.emobileconnect.service.impl.UserPlansServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserPlansServiceImplTest extends EMobileConnectApplication {
	private static final Logger logger=LoggerFactory.getLogger(UserPlansServiceImplTest.class);
	
	@InjectMocks
	UserPlansServiceImpl userPlansServiceImpl;
	
	@Mock
	UserPlansRepository userPlansRepository;
	
	
	static UserPlansDto userplans;
	
	@BeforeAll
	public static void setup() {
		userplans=new UserPlansDto();
		userplans.setRequestId(1);
		userplans.setRequestStatus("Pending");
		
		
	}
	
	@Test
	@DisplayName("UserPlans Service Test")
	public void userPlansServiceTest() throws UserPlansNotFoundException{
		
		when(userPlansRepository.findUserPlansByRequestId(1)).thenReturn(userplans);
		
		UserPlans response=userPlansServiceImpl.getUserPlansbyRequestId(1);
		
		assertNotEquals(userplans, response);
		logger.info("UserPlans Service Tested Successfully");
		
	}
	

}
