package com.emobileconnect.Controllertest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.emobileconnect.EMobileConnectApplication;
import com.emobileconnect.controller.UserPlansController;
import com.emobileconnect.entity.UserPlans;
import com.emobileconnect.exceptions.UserPlansNotFoundException;
import com.emobileconnect.service.UserPlansService;

@ExtendWith(MockitoExtension.class)
public class UserPlansControllerTest extends EMobileConnectApplication {
	private static final Logger logger=LoggerFactory.getLogger(UserPlansControllerTest.class);
	
	@Mock
	UserPlansService userPlansService;
	
	@InjectMocks
	UserPlansController userPlanController;
	
	
	static UserPlans userPlans;
	@BeforeAll
	public static void setUp() {
		userPlans=new UserPlans();
		((UserPlans) userPlans).setRequestId(1);
		((UserPlans) userPlans).setRequestStatus("Pending");
		
		
	}
	
	@Test
	@DisplayName("UserPlans Controller Test")
	public void userPlansTest() throws UserPlansNotFoundException  {
		
		when(userPlansService.getUserPlansbyRequestId(1)).thenReturn(userPlans);
		
		ResponseEntity<UserPlans> result=userPlanController.getUserPlansbyRequestId(1);
		
		assertEquals(userPlans, result.getBody());
		
		logger.info("UserPlans", result.getBody());
	}
}
